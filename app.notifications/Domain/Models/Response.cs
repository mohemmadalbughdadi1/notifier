﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace app.notifications.Domain.Models
{
    public interface IResponse { }
    public class Response<T> : IResponse
    {
        public Response()
        {

        }
        public Response(T data, bool status)
        {
            Data = data;
            Status = status;
        }
        public T Data { get; set; }
        public bool Status { get; set; }
    }
    public class StringResponse : Response<string>
    {
        public StringResponse(string response, bool status = true) : base(response, status)
        {

        }
        public static StringResponse Successful = new StringResponse("Successful");
        public static StringResponse Failed = new StringResponse("Failed", false);
    }
}
