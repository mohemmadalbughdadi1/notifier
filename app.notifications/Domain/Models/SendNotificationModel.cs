﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace app.notifications.Domain.Models
{
    public enum NotificationType { Email, SMS }
    public class SendNotificationModel
    {
        public string TemplateId { get; set; }
        public NotificationModel Notification { get; set; }
        public NotificationType NotificationType { get; set; }
    }
    public class NotificationModel
    {
        public NotificationModel()
        {
            ToClients = new List<NotificationClient> { };
        }
        public string Subject { get; set; }
        public IEnumerable<NotificationClient> ToClients { get; set; }
        public object Parameters { get; set; }
    }
    public class NotificationClient
    {
        public string Email { get; set; }
        public string Name { get; set; }
    }
}
