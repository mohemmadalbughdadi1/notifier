﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace app.notifications.Domain
{
    public static class Extentions
    {
        public static Dictionary<string, TValue> ToDictionary<TValue>(this object obj)
        {
            if (obj is null)
                return null;
            var json = JsonConvert.SerializeObject(obj);
            var dictionary = JsonConvert.DeserializeObject<Dictionary<string, TValue>>(json);
            return dictionary;
        }
    }
}
