﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using app.notifications.Domain.Models;
using SendGrid.Helpers.Mail;
using SendGrid;
using Microsoft.Extensions.Options;
using app.notifications.Domain.Common;
using FormatWith;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http.Json;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Hosting;
using System.IO;

namespace app.notifications.Domain.Providers
{
    public interface IEmailProvider
    {
        Task<IResponse> SendMailAsync(SendNotificationModel model);
    }
    public class SendGridEmailProvider : IEmailProvider
    {
        readonly IOptions<Settings> _options;
        readonly IWebHostEnvironment _env;

        public SendGridEmailProvider(IOptions<Settings> options, IWebHostEnvironment env)
        {
            _options = options;
            _env = env;
        }

        public async Task<IResponse> SendMailAsync(SendNotificationModel model)
        {
            try
            {
                var templatePath = Path.Combine(_env.WebRootPath, "Templates", $"{model.TemplateId}.html");
                var template = File.ReadAllText(templatePath);

                var sendGridOption = _options.Value.ExternalProviders.SendGrid;
                var client = new SendGridClient(sendGridOption.Key);
                var from = new EmailAddress(sendGridOption.Email, sendGridOption.Name);
                var to = model.Notification.ToClients.Select(x => new EmailAddress(x.Email, x.Name)).ToList();
                var htmlContent = template.FormatWith(model.Notification.Parameters.ToDictionary<string>(),MissingKeyBehaviour.Ignore);
                var msg = MailHelper.CreateSingleEmail(from, to.FirstOrDefault(), model.Notification.Subject, "", htmlContent);
                var response = await client.SendEmailAsync(msg);
                if (!response.IsSuccessStatusCode)
                    return StringResponse.Failed;

                return StringResponse.Successful;
            }
            catch(Exception e)
            {
                return StringResponse.Failed;
            }
        }
    }
}
