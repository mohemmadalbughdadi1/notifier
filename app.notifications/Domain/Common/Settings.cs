﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace app.notifications.Domain.Common
{
    public class Settings
    {
        public ExternalProvidersOptions ExternalProviders { get; set; }
    }
    public class ExternalProvidersOptions
    {
        public SendGridOptions SendGrid { get; set; }
    }
    public class SendGridOptions
    {
        public string Key { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
    }
}
