﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using app.notifications.Domain.Providers;
using Microsoft.Extensions.DependencyInjection;

namespace app.notifications.Configuration
{
    public static class ProviderConfiguration
    {
        public static void AddProviders(this IServiceCollection services)
        {
            services.AddScoped<IEmailProvider, SendGridEmailProvider>();
        }
    }
}
