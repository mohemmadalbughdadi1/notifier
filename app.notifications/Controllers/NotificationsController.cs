﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using app.notifications.Domain;
using app.notifications.Domain.Models;
using app.notifications.Domain.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace app.notifications.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotificationsController : ControllerBase
    {
        readonly IEmailProvider _emailProvider;
        public NotificationsController(IEmailProvider emailProvider)
        {
            _emailProvider = emailProvider;
        }
        [HttpPost]
        public async Task<IActionResult> Send(SendNotificationModel model)
        {
            return new JsonResult(await _emailProvider.SendMailAsync(model));
        }
    }
}
